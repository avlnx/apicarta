# User Authorization API: /api/dm/login

## POST /api/dm/login with invalid username/password combination:

    curl --location --request POST 'localhost:5000/api/dm/login' \
    --header 'Content-Type: application/json' \
    --data-raw '{"username": "badusername", "password": "badpassword"}'

401 Response:

    {
      "msg": "Invalid username or password"
    }

## POST /api/dm/login with valid username/password combination returns a token:

    curl --location --request POST 'localhost:5000/api/dm/login' \
    --header 'Content-Type: application/json' \
    --data-raw '{"username": "davidjohnson", "password": "Welcome123$"}'

200 Response:

    {
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MTE2ODY1MTcsIm5iZiI6MTYxMTY4NjUxNywianRpIjoiNjdkNjFiYzQtNWE4My00YTI4LWE3NzUtZTI3MmQ3MTE1MjBmIiwiZXhwIjoxNjExNjg3NDE3LCJpZGVudGl0eSI6ImRhdmlkam9obnNvbiIsImZyZXNoIjpmYWxzZSwidHlwZSI6ImFjY2VzcyJ9.7XE4arAy-5MYjnN_pEg6DsCosP0txF7ZHBU-R-_GVew"
    }
