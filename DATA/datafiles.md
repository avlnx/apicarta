# Data Management API

## POST /api/dm/datafiles with proper auth token header:

- Ps. If you would like to run this yourself please update the file path in the --form header. Another option would be
  to use the awesome [Postman](https://www.postman.com/).

    curl --location --request POST 'localhost:5000/api/dm/datafiles' \
    --header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MTE3MDExMDYsIm5iZiI6MTYxMTcwMTEwNiwianRpIjoiNzNlZjc0NzYtZjkwYS00OGZhLWEwZjgtY2RkNDIwMWUwYmFiIiwiZXhwIjoxNjExNzAyMDA2LCJpZGVudGl0eSI6ImRhdmlkam9obnNvbiIsImZyZXNoIjpmYWxzZSwidHlwZSI6ImFjY2VzcyJ9.LwPOMIx4SqAjcsxTF4Z9zH2EvaJ5IqrCUjb5nm--DBg'
    --form 'file=@"/Users/avlnx9/dev/carta/solution/apicarta/tests/fixtures/data-test.csv"'

### 200 Response:

    {
        "msg": "File data-test.csv uploaded successfully"
    }

## GET /api/dm/datafiles with proper auth token header:

    curl --location --request GET 'localhost:5000/api/dm/datafiles' \
    --header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MTE3MDIxODIsIm5iZiI6MTYxMTcwMjE4MiwianRpIjoiYWU3ZjkwZWUtYWE3OS00M2Y0LTk5ZDctNTZmMTE4ZmVlNjE3IiwiZXhwIjoxNjExNzAzMDgyLCJpZGVudGl0eSI6ImRhdmlkam9obnNvbiIsImZyZXNoIjpmYWxzZSwidHlwZSI6ImFjY2VzcyJ9.ueAW_fjdnWx12FHOu0vOj-XO8rdfltDp3urKw9dH6RU'

### 200 Response:

    {
      "items": [
        {
          "created_time": "2021-01-26T17:29:28.058406", 
          "data_file_name": "data-test.csv", 
          "fail_count": 0, 
          "id": 4, 
          "success_count": 4, 
          "total_count": 4, 
          "user_id": 2
        }, 
        {
          "created_time": "2021-01-26T22:48:44.095242", 
          "data_file_name": "data-test.csv", 
          "fail_count": 0, 
          "id": 5, 
          "success_count": 4, 
          "total_count": 4, 
          "user_id": 2
        }
      ], 
      "next": null, 
      "page": 1, 
      "pages": 1, 
      "per_page": 10, 
      "prev": null, 
      "total": 2
    }

## GET /api/dm/datafiles/4 with proper auth token header:

- Ps. Note the token belongs to the owner of this datafile with id 4

    curl --location --request GET 'localhost:5000/api/dm/datafiles/4' \
    --header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MTE3MDIxODIsIm5iZiI6MTYxMTcwMjE4MiwianRpIjoiYWU3ZjkwZWUtYWE3OS00M2Y0LTk5ZDctNTZmMTE4ZmVlNjE3IiwiZXhwIjoxNjExNzAzMDgyLCJpZGVudGl0eSI6ImRhdmlkam9obnNvbiIsImZyZXNoIjpmYWxzZSwidHlwZSI6ImFjY2VzcyJ9.ueAW_fjdnWx12FHOu0vOj-XO8rdfltDp3urKw9dH6RU'

### 200 Response:

    {
      "items": [
        {
          "created_time": "2021-01-26T17:29:28.076764", 
          "data_value": 728239, 
          "data_year": 2019, 
          "id": 61673, 
          "industry_aggregation_nzsioc": "Level 1", 
          "industry_code_anzsic06": "ANZSIC06 divisions A-S (excluding classes K6330, L6711, O7552, O760, O771, O772, S9540, S9601, S9602, and S9603)", 
          "industry_code_nzsioc": "99999", 
          "industry_name_nzsioc": "All industries", 
          "units": "Dollars (millions)", 
          "user_data_id": 4, 
          "variable_category": "Financial performance", 
          "variable_code": "H01", 
          "variable_name": "Total income"
        }, 
        {
          "created_time": "2021-01-26T17:29:28.076764", 
          "data_value": 643809, 
          "data_year": 2019, 
          "id": 61674, 
          "industry_aggregation_nzsioc": "Level 1", 
          "industry_code_anzsic06": "ANZSIC06 divisions A-S (excluding classes K6330, L6711, O7552, O760, O771, O772, S9540, S9601, S9602, and S9603)", 
          "industry_code_nzsioc": "99999", 
          "industry_name_nzsioc": "All industries", 
          "units": "Dollars (millions)", 
          "user_data_id": 4, 
          "variable_category": "Financial performance", 
          "variable_code": "H04", 
          "variable_name": "Sales, government funding, grants and subsidies"
        }, 
        {
          "created_time": "2021-01-26T17:29:28.076764", 
          "data_value": 62924, 
          "data_year": 2019, 
          "id": 61675, 
          "industry_aggregation_nzsioc": "Level 1", 
          "industry_code_anzsic06": "ANZSIC06 divisions A-S (excluding classes K6330, L6711, O7552, O760, O771, O772, S9540, S9601, S9602, and S9603)", 
          "industry_code_nzsioc": "99999", 
          "industry_name_nzsioc": "All industries", 
          "units": "Dollars (millions)", 
          "user_data_id": 4, 
          "variable_category": "Financial performance", 
          "variable_code": "H05", 
          "variable_name": "Interest, dividends and donations"
        }, 
        {
          "created_time": "2021-01-26T17:29:28.076764", 
          "data_value": 21505, 
          "data_year": 2019, 
          "id": 61676, 
          "industry_aggregation_nzsioc": "Level 1", 
          "industry_code_anzsic06": "ANZSIC06 divisions A-S (excluding classes K6330, L6711, O7552, O760, O771, O772, S9540, S9601, S9602, and S9603)", 
          "industry_code_nzsioc": "99999", 
          "industry_name_nzsioc": "All industries", 
          "units": "Dollars (millions)", 
          "user_data_id": 4, 
          "variable_category": "Financial performance", 
          "variable_code": "H07", 
          "variable_name": "Non-operating income"
        }
      ], 
      "next": null, 
      "page": 1, 
      "pages": 1, 
      "per_page": 10, 
      "prev": null, 
      "total": 4
    }

## GET /api/dm/datafiles or /api/dm/datafiles/<id> without valid auth token header:

    curl --location --request POST 'localhost:5000/api/dm/login' \
    --header 'Content-Type: application/json' \
    --data-raw '{"username": "badusername", "password": "badpassword"}'

### 401 Response:

    {
        "msg": "Missing Authorization Header"
    }

## GET /api/dm/datafiles or /api/dm/datafiles/<id> with invalid auth token header:

    curl --location --request GET 'localhost:5000/api/dm/datafiles' \
    --header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MTE3MDExMDYsIm5iZiI6MTYxMTcwMTEwNiwianRpIjoiNzNlZjc0NzYtZjkwYS00OGZhLWEwZjgtY2RkNDIwMWUwYmFiIiwiZXhwIjoxNjExNzAyMDA2LCJpZGVudGl0eSI6ImRhdmlkam9obnNvbiIsImZyZXNoIjpmYWxzZSwidHlwZSI6ImFjY2VzcyJ9.LwPOMIx4SqAjcsxTF4Z9zH2EvaJ5IqrCUjb5nm--DBG'

### 401 Response:

    {
        "msg": "Signature verification failed"
    }

## POST /api/dm/datafiles without valid auth token header:

    curl --location --request POST 'localhost:5000/api/dm/datafiles' \
    --form 'file=@"/Users/avlnx9/dev/carta/solution/apicarta/tests/fixtures/data-test.csv"'

### 401 Response:

    {
        "msg": "Missing Authorization Header"
    }
