FROM continuumio/miniconda:latest

# disabled for development
# RUN apt-get update -y && \
#    apt-get upgrade -y

RUN mkdir /app

RUN mkdir /app/uploads

WORKDIR /app

COPY environment.yml ./

RUN conda env create -f environment.yml

RUN echo "conda activate apicarta" >> ~/.bashrc
ENV PATH /opt/conda/envs/apicarta/bin:$PATH

COPY . /app

ENV FLASK_APP=api
ENV FLASK_ENV=development

ENTRYPOINT [ "python" ]

CMD [ "run.py" ]

EXPOSE 5000

