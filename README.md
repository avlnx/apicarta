# Carta Healthcare Flask App Test

### Requirements to run the app

- [Docker](https://docs.docker.com/get-docker/)

## How to run the app

- [Install Docker](https://docs.docker.com/get-docker/) locally if missing.

- Clone this application:

  `git clone https://gitlab.com/avlnx/apicarta.git`

- Change into the app's directory:

  `cd apicarta`

- Run the docker-compose command:

  `docker-compose up`

- Wait for the images to be built. This can take a while because we need to install all dependencies. 
  Eventually you should see some output like this:


      api            |  * Serving Flask app "api" (lazy loading)
      api            |  * Environment: development
      api            |  * Debug mode: on
      api            |  * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
      api            |  * Restarting with stat
      api            |  * Debugger is active!
      api            |  * Debugger PIN: 332-710-921


- This means the api is ready and available at `http://localhost:5000`. You can interact with it with an app like [Postman](https://www.postman.com/)
  or with `curl`. See `/DATA/auth.md` and `/DATA/datafiles.md` for some examples. The databse is automatically seeded
  with the provided sql (users).
  
## Running tests

- Make sure the containers are running (see above) and run the following command from the root of the project:

  `docker-compose exec api python -m pytest`

### Troubleshooting

- If you run into issues running the docker containers you might be running out of memory. You will have to increase it.
- See [this docker forums post](https://forums.docker.com/t/how-to-increase-memory-size-that-is-available-for-a-docker-container/78483)
- This [stackoverflow answer](https://stackoverflow.com/questions/43460770/docker-windows-container-memory-limit) might
  help too.

### Author

Thyago F. da Silva [thferreira@gmail.com](mailto:thferreira@gmail.com)