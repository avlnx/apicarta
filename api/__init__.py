from flask import Flask
from flask_jwt_extended import JWTManager
from flask_bcrypt import Bcrypt
from .models import db, User, ma
from api.config import config


def create_app(env="dev"):
    app = Flask(__name__)

    # to use test config inject "test" as env
    # in prod we could use an actual environment variable
    app.config.from_object(config[env])

    db.init_app(app)
    ma.init_app(app)

    with app.app_context():
        jwt = JWTManager(app)
        bcrypt = Bcrypt(app)
        db.create_all()
        seed_users()

        from . import auth, datafiles
        app.register_blueprint(auth.bp, url_prefix='/api/dm/login')
        app.register_blueprint(datafiles.bp, url_prefix='/api/dm/datafiles')

        return app


def seed_users():
    # imported from data-manager.sql
    user_seeds = [
        ('John', 'Smith', 'johnsmith', '$2b$12$zHlI8ZpxyWYgdjxQadIU6ebDKioD5KmK1pYJumfRzVND10aGi9PVq'),
        ('David', 'Johnson', 'davidjohnson', '$2b$12$zHlI8ZpxyWYgdjxQadIU6ebDKioD5KmK1pYJumfRzVND10aGi9PVq'),
        ('Amir', 'Strum', 'amirstrum', '$2b$12$zHlI8ZpxyWYgdjxQadIU6ebDKioD5KmK1pYJumfRzVND10aGi9PVq'),
        ('Avi', 'Galber', 'avigelber', '$2b$12$zHlI8ZpxyWYgdjxQadIU6ebDKioD5KmK1pYJumfRzVND10aGi9PVq'),
    ]

    first_user_name = user_seeds[0][2]
    found = User.query.filter_by(user_name=first_user_name).first()
    if found is None:
        # first time around, seed new users
        for user in user_seeds:
            db.session.add(User(first_name=user[0], last_name=user[1], user_name=user[2], user_pwd=user[3]))
        db.session.commit()
