from flask import Blueprint, request, jsonify
from flask_jwt_extended import create_access_token
from .models import User

bp = Blueprint('auth', __name__)


@bp.route('', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    json_data = request.get_json()
    username = json_data['username']
    password = json_data['password']

    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400

    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    u = User.query.filter_by(user_name=username).first()
    if (u is None) or (not u.check_password(password)):
        return jsonify({"msg": "Invalid username or password"}), 401

    access_token = create_access_token(identity=username)
    return jsonify(access_token=access_token), 200
