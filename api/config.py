import os


class Config:
    """ Base Configuration """
    SECRET_KEY = os.environ.get('SECRET_KEY')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY')
    UPLOAD_FOLDER = '/app/uploads'
    ALLOWED_EXTENSIONS = {'csv'}


class DockerDevConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:carta@postgres/data_manager'
    DEBUG = True


class TestingConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:carta@postgres-test:5433/test_data_manager'
    TESTING = True


config = {"dev": DockerDevConfig, "test": TestingConfig}
