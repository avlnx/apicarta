from flask import Blueprint, request, jsonify, current_app
from werkzeug.utils import secure_filename
from flask_jwt_extended import jwt_required, get_jwt_identity
from .models import Dataset, User, db, DatasetSchema, DatasetDetail, DatasetDetailSchema
import os
import pandas as pd
from sqlalchemy.types import BigInteger, Integer, String, DateTime
import datetime


bp = Blueprint('datafiles', __name__)


@bp.route('', methods=['GET', 'POST'])
@jwt_required
def datafiles():
    user = get_current_user()
    if user is None:
        return jsonify({"msg": 'Permission denied.'}), 400

    if request.method == 'GET':
        return get_user_files(user)
    else:
        return add_new_dataset_for(user)


@bp.route('/<int:file_id>', methods=['GET'])
@jwt_required
def datafile_details(file_id):
    dataset = Dataset.query.get(file_id)
    if not dataset:
        return jsonify({"msg": "File not found."}), 404

    if not current_user_owns(dataset):
        return jsonify({"msg": "Permission denied."}), 401

    return get_datafile_details(file_id)


def get_user_files(user):
    # show list of this user's datasets
    page = request.args.get('page') or 1
    datasets = Dataset.query.filter_by(user_id=user.id).paginate(int(page), 10, error_out=False)
    dataset_schema = DatasetSchema()

    return jsonify(paginate_object_list(datasets, dataset_schema)), 200


def add_new_dataset_for(user):
    # check if the post request has the file part and a file was actually chosen
    if 'file' not in request.files or request.files['file'].filename == '':
        return jsonify({"msg": "Invalid request. File is required."}), 400

    file = request.files['file']
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))
        process_file_and_make_dataset_model_instance(filename, user.id)
        return jsonify({"msg": f'File {filename} uploaded successfully'}), 200
    else:
        return jsonify({"msg": "Invalid file."}), 400


def process_file_and_make_dataset_model_instance(data_file_name, user_id):
    df = pd.read_csv(os.path.join(current_app.config['UPLOAD_FOLDER'], data_file_name),
                     converters={"Value": parse_data_value})
    dataset = create_dataset_model_instance_from_dataframe(df, data_file_name, user_id)
    create_dataset_details_model_instances_from_dataframe(df, dataset)


def create_dataset_model_instance_from_dataframe(df, data_file_name, user_id):
    total_count = df.shape[0]
    fail_count = df["Value"].isna().sum()
    success_count = total_count - fail_count

    dataset = Dataset(
        data_file_name=data_file_name,
        user_id=user_id,
        total_count=total_count,
        fail_count=int(fail_count),
        success_count=int(success_count)
    )
    db.session.add(dataset)
    db.session.commit()

    return dataset


def create_dataset_details_model_instances_from_dataframe(df, dataset):
    df = df.dropna()
    df["Value"] = df["Value"].astype(int)
    df['user_data_id'] = dataset.id
    df['created_time'] = datetime.datetime.utcnow()
    df = df.rename(str.lower, axis='columns')
    df = df.rename(columns={"year": "data_year", "value": "data_value"})
    df.to_sql('user_data_details',
              con=db.engine,
              if_exists='append',
              index=False,
              chunksize=1000,
              dtype={
                  "data_year": Integer,
                  "industry_aggregation_nzsioc": String,
                  "industry_code_nzsioc": String,
                  "industry_name_nzsioc": String,
                  "units": String,
                  "variable_code": String,
                  "variable_name": String,
                  "variable_category": String,
                  "data_value": Integer,
                  "industry_code_anzsic06": String,
                  "user_data_id": BigInteger,
                  "created_time": DateTime
              })


def current_user_owns(dataset):
    return get_current_user().id == dataset.user_id


def get_datafile_details(file_id):
    page = request.args.get('page') or 1
    dataset_details = DatasetDetail.query.filter_by(user_data_id=file_id).paginate(int(page), 10, error_out=False)
    dataset_details_schema = DatasetDetailSchema()
    return jsonify(paginate_object_list(dataset_details, dataset_details_schema)), 200


def get_current_user():
    user_name = get_jwt_identity()
    user = User.query.filter_by(user_name=user_name).first()
    return user


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in current_app.config['ALLOWED_EXTENSIONS']


def parse_data_value(val):
    return pd.to_numeric(val.replace(",", ""), downcast='integer', errors='coerce')


def paginate_object_list(pagination_object, schema):
    return {
        "total": pagination_object.total,
        "page": pagination_object.page,
        "prev": pagination_object.prev_num,
        "next": pagination_object.next_num,
        "pages": pagination_object.pages,
        "per_page": pagination_object.per_page,
        "items": [schema.dump(o) for o in pagination_object.items]
    }
