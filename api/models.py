import datetime
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import check_password_hash
from flask_marshmallow import Marshmallow

db = SQLAlchemy()
ma = Marshmallow()


class User(db.Model):
    """ Data model for users. """
    __tablename__ = 'users'
    id = db.Column(db.BigInteger, primary_key=True)
    first_name = db.Column(db.String, nullable=False)
    last_name = db.Column(db.String, nullable=False)
    user_name = db.Column(db.String, nullable=False, unique=True)
    user_pwd = db.Column(db.String, nullable=False)
    is_active = db.Column(db.Boolean, default=True)
    created_time = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    datasets = db.relationship('Dataset', backref='user', lazy=True)

    def check_password(self, password):
        return check_password_hash(self.user_pwd, password)


class Dataset(db.Model):
    """ Data model for user uploaded datasets """
    __tablename__ = 'user_data'
    id = db.Column(db.BigInteger, primary_key=True)
    data_file_name = db.Column(db.String, nullable=False)
    total_count = db.Column(db.Integer, nullable=False)
    success_count = db.Column(db.Integer, nullable=False)
    fail_count = db.Column(db.Integer, nullable=False)
    user_id = db.Column(db.BigInteger, db.ForeignKey('users.id'), nullable=False)
    created_time = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    dataset_details = db.relationship('DatasetDetail', backref='dataset', lazy=True)


class DatasetDetail(db.Model):
    """ Data model for dataset details """
    __tablename__ = 'user_data_details'
    id = db.Column(db.BigInteger, primary_key=True)
    data_year = db.Column(db.Integer, nullable=False)
    industry_aggregation_nzsioc = db.Column(db.String, nullable=False)
    industry_code_nzsioc = db.Column(db.String, nullable=False)
    industry_name_nzsioc = db.Column(db.String, nullable=False)
    units = db.Column(db.String, nullable=False)
    variable_code = db.Column(db.String, nullable=False)
    variable_name = db.Column(db.String, nullable=False)
    variable_category = db.Column(db.String, nullable=False)
    data_value = db.Column(db.Integer, nullable=False)
    industry_code_anzsic06 = db.Column(db.String, nullable=False)
    user_data_id = db.Column(db.BigInteger, db.ForeignKey('user_data.id'), nullable=False)
    created_time = db.Column(db.DateTime, default=datetime.datetime.utcnow)


class DatasetSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Dataset
        include_fk = True


class DatasetDetailSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = DatasetDetail
        include_fk = True
