from api import create_app
import pytest
from api.models import Dataset, DatasetDetail, db

env = "test"


@pytest.fixture
def app():
    app = create_app(env)
    yield app


@pytest.fixture
def client(app):
    with app.test_client() as client:
        with app.app_context():
            yield client
    # clear db after tests
    reset_db(app)


def reset_db(app):
    with app.app_context():
        db.session.query(DatasetDetail).delete()
        db.session.query(Dataset).delete()
        db.session.commit()
        db.session.close()


@pytest.fixture
def valid_credentials():
    return {'username': 'johnsmith', 'password': 'Welcome123$'}


@pytest.fixture
def get_valid_token(client, valid_credentials):
    username = valid_credentials['username']
    password = valid_credentials['password']
    response = client.post('/api/dm/login', json={
        'username': username,
        'password': password
    }, follow_redirects=True)
    return response.get_json()
