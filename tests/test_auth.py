def login(client, username, password):
    return client.post('/api/dm/login', json={
        'username': username,
        'password': password
    }, follow_redirects=True)


def test_login(client, valid_credentials):
    # a request with invalid credentials should return a 4** response
    response = login(client, 'bad_username', 'bad_password')
    assert response.status_code == 401

    # a request with proper credentials should return an access token and a 200 code
    response = login(client, valid_credentials['username'], valid_credentials['password'])
    assert response.status_code == 200
    assert 'access_token' in response.get_json()
