import os
from api.models import User, Dataset, DatasetDetail


def test_access_only_if_authenticated(client):
    # should get a 4** response if no token was sent in
    response = client.get('/api/dm/datafiles', follow_redirects=True)
    assert response.status_code == 401

    # same for a details endpoint
    response = client.get('/api/dm/datafiles/1', follow_redirects=True)
    assert response.status_code == 401


def test_access_granted_if_authenticated(client, get_valid_token):
    token = get_valid_token["access_token"]
    response = client.get('/api/dm/datafiles', follow_redirects=True, headers={
        'Authorization': f'Bearer {token}'
    })
    assert response.status_code == 200


def test_file_upload_generates_proper_models(client, get_valid_token):
    token = get_valid_token["access_token"]
    data = {}
    test_fixture_file_path = os.path.join(os.path.dirname(__file__), 'fixtures/data-test.csv')
    with open(test_fixture_file_path, 'rb') as f:
        data['file'] = (f, f.name)
        response = client.post('/api/dm/datafiles',
                               content_type='multipart/form-data',
                               data=data,
                               headers={
                                   'Authorization': f'Bearer {token}'
                               })
    assert response.status_code == 200
    response_json = response.get_json()
    assert "uploaded successfully" in response_json['msg']

    # test we have expected model instances in db
    datasets = Dataset.query.all()
    assert len(datasets) == 1
    dataset_details = DatasetDetail.query.all()
    assert len(dataset_details) == 4

    # test we can query the api for the newly created dataset
    response = client.get('/api/dm/datafiles', follow_redirects=True, headers={
        'Authorization': f'Bearer {token}'
    })
    response_json = response.get_json()
    assert "items" in response_json
    assert len(response_json["items"]) == 1

    dataset_id = response_json["items"][0]["id"]

    # test we can get the new dataset dataset_details as well
    response = client.get(f'/api/dm/datafiles/{dataset_id}', follow_redirects=True, headers={
        'Authorization': f'Bearer {token}'
    })
    assert response.status_code == 200
    response_json = response.get_json()
    assert "items" in response_json
    assert len(response_json["items"]) == 4
