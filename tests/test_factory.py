from api import create_app


def test_config():
    # if not testing should default to "dev" env
    assert not create_app().testing

    # if "test" env, should be testing
    assert create_app("test").testing
